create database DB_sales295

create table salesperson(
id int primary key identity(1,1) not null,
name varchar(255)not null,
bod date ,
salary decimal(18,2) not null
)

insert into salesperson(name,bod,salary) 
values
('Abe','9/11/1998','140000'),
('Bob','9/11/1978','44000'),
('Chris','9/11/1983','40000'),
('Dan','9/11/1980','52000'),
('Ken','9/11/1977','115000'),
('Joe','9/11/1990','38000')

update salesperson set salary = '38000' where id =6

select * from salesperson

create table orders(
id int primary key identity(1,1)not null,
order_date date,
cust_id int not null,
salesperson_id int not null,
amount decimal(18,2) not null
)

insert into orders(order_date,cust_id,salesperson_id,amount) 
values 
('8/02/2020','4','2','540'),
('1/22/2021','4','5','1800'),

('7/14/2019','9','1','460'),
('1/29/2018','7','2','2400'),
('2/3/2021','6','4','600'),
('3/2/2020','6','4','720'),
('5/6/2021','9','4','150')


select * from orders
select * from salesperson


-- a. Informasi nama sales yang memiliki order lebih dari 1.
select s.name, count(s.name) as jml_order from orders as o
join salesperson as s
on o.salesperson_id = s.id
group by  o.salesperson_id,s.name
having count(o.cust_id )> 1


--  b. Informasi nama sales yang total amount ordernya di atas 1000.
select s.name,sum(o.amount) as total from orders as o
join salesperson as s
on o.salesperson_id = s.id
group by s.name having sum(o.amount) > 1000

--c. Informasi nama sales, umur, gaji dan total amount order yang tahun ordernya >= 2020 dan data ditampilan berurut sesuai dengan umur (ascending)
select s.name,DATEDIFF(year,s.bod,GETDATE()) as umur,s.salary,
sum(o.amount) as total
from orders as o
join salesperson as s
on o.salesperson_id = s.id
where year(o.order_date) >= 2020
group by s.name,s.salary,s.bod
order by DATEDIFF(year,s.bod,GETDATE()) 

-- d. Carilah rata-rata total amount masing-masig sales urutkan dari hasil yg paling besar
select s.name,
avg(o.amount) as total
from orders as o
join salesperson as s
on o.salesperson_id = s.id
group by s.name
order by avg(o.amount) desc

-- e. perusahaan akan memberikan bonus bagi sales yang berhasil memiliki order lebih dari 2 dan total order lebih dari 1000 sebanyak 30% dari salary
select s.name, count(s.name) as jml_order,
(s.salary * 0.3) as bonus,
sum(o.amount) as total_amount,
((s.salary * 0.3)+s.salary) as Gatot

from orders as o
join salesperson as s
on o.salesperson_id = s.id
group by o.salesperson_id,s.name,s.salary
having count(o.cust_id )> 2 and sum(o.amount) > 1000

-- f. Tampilkan data sales yang belum memiliki orderan sama sekali
select s.name,s.bod,s.salary
from orders as o
right join salesperson as s
on o.salesperson_id = s.id
where o.salesperson_id is null

-- g.Gaji sales akan dipotong jika tidak memiliki orderan,  gaji akan di potong sebanyak 2%
select s.name,s.salary,
s.salary * 0.02 as potongan,
s.salary - (s.salary * 0.02) as gatot

from orders as o
right join salesperson as s
on o.salesperson_id = s.id
where o.salesperson_id is null
group by s.name,s.salary



-- a. Informasi nama sales yang memiliki order lebih dari 1.

select * from salesperson
select * from orders

select s.name, count(s.name) as jml_order
from salesperson as s
join orders as o
on o.salesperson_id = s.id
group by s.name
having count(s.name)> 1

-- b. Informasi nama sales yang total amount ordernya di atas 1000.

select s.name,sum(o.amount) as total_amount from salesperson as s
join orders as o 
on o.salesperson_id = s.id
group by s.name
having sum(o.amount) > 1000

--c.


