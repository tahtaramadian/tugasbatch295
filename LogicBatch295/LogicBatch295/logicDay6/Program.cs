﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace logicDay6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Masukan Pilihan soal 1-5 : ");
            int pilih = int.Parse(Console.ReadLine());

            switch (pilih)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
                case 5:
                    soal5();
                    break;

            }


            Console.ReadKey();
        }
        static void soal2()
        {


            Console.Write("Masukan Kalimat = ");
            string kalimat = Console.ReadLine();
            Console.Write("Masukan Jumlah Rotate = ");
            int rotate = int.Parse(Console.ReadLine());

            string newKalimat = "";
            string alphabet = "abcdefghijklmnopqrstuvwxyz";
            string upAlpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string newAlphabet = "";
            string newAlphabetup = " ";
            string simpan = alphabet;
            string simpanup = upAlpha;

            for (int i = 0; i < rotate; i++)
            {
                newAlphabet = simpan.Substring(1, simpan.Length - 1) + simpan[0]; // BCD.....ZA //CD....ZAB
                simpan = newAlphabet;
            }
            for (int i = 0; i < rotate; i++)
            {
                newAlphabetup = simpanup.Substring(1, simpanup.Length - 1) + simpanup[0]; // BCD.....ZA //CD....ZAB
                                                                                 simpanup = newAlphabetup;
            }
            foreach (char huruf in kalimat) //BA
            {
                int indeksUp = upAlpha.IndexOf(huruf);
                int indeks = alphabet.IndexOf(huruf); //B = 1 , A = 0
                if (indeks != -1)
                {
                    newKalimat += newAlphabet[indeks]; //DC
                }
                else if (indeksUp != -1)
                {
                    newKalimat += newAlphabetup[indeksUp];
                }
                else
                {
                    newKalimat += huruf;
                }
            }
            Console.WriteLine($"Alphabet = {alphabet}");
            Console.WriteLine($"Alphabet Baru = {newAlphabet}");
            Console.WriteLine($"Kalimat Baru = {newKalimat}");

        }

        static void soal1()
        {
            int batas = 0;
            int lembah = 0;
            Console.Write("masukan input = ");
            string input = Console.ReadLine();

            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] == 'D')
                {
                    batas--;
                }
                else if (input[i] == 'U')
                {
                    batas++;
                }
                if (input[i] == 'D' && batas == -1)
                {
                    lembah++;
                }

            }
            Console.Write(lembah);
        }



        static void soal3() //input == dia DAN DI CARI BERDASARKAN INDEX // 1 3 1 4 6 2 1 1 3 5 2 3 1 1 1 1 5 2 3 1 3 5 4 3 2 5
        {
            Console.Write("Input Tinggi: ");
            int[] tinggi = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);// deteksi soal itu adalah angka, maka convert array, kemudian di split agar mendapatkan indexnya, dan di parse karena di awal sifatnya console input itu string
            Console.Write("input Text Kalimat : ");
            string textKalimat = Console.ReadLine().ToLower();

            //buat deteksi untuk huruf yang nantinya akan di input dan di detec dengan data yang tersedia
            string alfabet = "abcdefghijklmnopqrstuvwxyz";
            // buat tempat penyimpanan dengan jenis List untuk inputan huruf baru
            List<int> simpan = new List<int>();

            //BUAT PERULANGAN UNTUK CEK TEXT KALIMAT AGAR INDEXNYA KEDETEKSI PADA TABEL STRING ALFABET YANG TERSEDIA
            for (int i = 0; i < textKalimat.Length; i++)
            {
                char huruf = textKalimat[i];//UNTUK detek dari inputan textKalimat pada index ke [i] di buat dalam variabel HURUF DENGAN JENIS CHAR
                int indeks = alfabet.IndexOf(huruf); // untuk membuat deteksi BARU dari hasil ALFABET YG TERSEDIA DAN DIDETEKSI INDEXNYA MILIK SI HURUF yang bersifat char tadi
                int nilai = tinggi[indeks];// ini untuk deteksi di setiap INDEKX PADA TINGGI 
                simpan.Add(nilai);

                //cetak
                Console.WriteLine($"Index Alfabet {huruf} = {indeks}, maka index ke {indeks} di elemen tinggi bernilai = {nilai}");
            }

            //buat elemen baru lagi untuk deteksi PANJANG/Length dan nilai MAX
            int lenght = textKalimat.Length;
            int max = simpan.Max();
            // cetak Hasil
            int hasil = max * lenght;
            Console.WriteLine($"{max} x {lenght} = {hasil}");
            //string tidak memerlukan perhitungan, jika perlu perhitungan WAJIB DIUBAH KE INT
        }

        static void soal4()
        {
            string vokal = "aiueoAIUEO";
            string konsonan = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ";

            Console.Write("Input = ");
            string input = Console.ReadLine();
            int x;
            int nilai = 0;

            Console.Write("\nHuruf Vokal = ");
            string[] arrayInput1 = new string[input.Length];
            for (x = 0; x < input.Length; x++)
            {
                nilai = vokal.IndexOf(input[x]);
                if (nilai > -1)
                {
                    arrayInput1[x] = input[x].ToString();
                }
            }
            Array.Sort(arrayInput1);
            foreach (string hasil in arrayInput1)
            {
                Console.Write(hasil);
            }
            Console.WriteLine();

            Console.Write("\nHuruf Konsonan = ");
            string[] arrayInput2 = new string[input.Length]; //untuk membuat tempat kosong 
            for (x = 0; x < input.Length; x++)
            {
                nilai = konsonan.IndexOf(input[x]);
                if (nilai > -1)
                {
                    arrayInput2[x] = input[x].ToString();
                }
            }
            Array.Sort(arrayInput2);
            foreach (string hasil in arrayInput2)
            {
                Console.Write(hasil);
            }
            Console.WriteLine();

        }

        static void soal5()
        {
            Console.Write("Password = ");
            string password = Console.ReadLine();

            string HurufBesar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string HurufKecil = "abcdefghijklmnopqrstuvwxyz";
            string Angka = "1234567890";
            string Simbol = "~!@#$%^&*()_+-={}[]|<>?/:;";
            int x, nilai;
            int hasil1 = 0;
            int hasil2 = 0;
            int hasil3 = 0;
            int hasil4 = 0;

            for (x = 0; x < password.Length; x++)
            {
                nilai = HurufBesar.IndexOf(password[x]);
                if (nilai > -1)
                {
                    hasil1++;


                }
                //if(HurufBesar && HurufKecil)
                //{

                //}

            }

            for (x = 0; x < password.Length; x++)
            {
                nilai = HurufKecil.IndexOf(password[x]);
                if (nilai > -1)
                {

                    hasil2++;

                }
            }

            for (x = 0; x < password.Length; x++)
            {
                nilai = Angka.IndexOf(password[x]);
                if (nilai > -1)
                {
                    hasil3++;

                }
            }

            for (x = 0; x < password.Length; x++)
            {
                nilai = Simbol.IndexOf(password[x]);
                if (nilai > -1)
                {
                    hasil4++;

                }

            }


            if (password.Length < 6)
            {
                Console.Write("Password Weak Dan Kurang dari 6 digit dan kurang symbol");
            }
            else
            {
                if (hasil1 > 0 && hasil2 > 0 && hasil3 > 0 && hasil4 > 0)
                {
                    Console.WriteLine("Password strong ");
                }
                else if (hasil1 > 0)
                {
                    Console.WriteLine("Pass weak kurang angka dan symbol");
                }
                else if (hasil2 > 0)
                {
                    Console.WriteLine("Pass weak kurang huruf besar dan symbol");
                }

                else if (hasil3 > 0)
                {
                    Console.WriteLine("Pas weak kurang buruf besar dan angka ");

                }
            }

        }
    }
}



