﻿using System;

namespace logicDay3
{
    class Program
    {
        static void Main(string[] args)
        {
            string ulang = "";
            do
            {
            Console.Write("1. DIM 1 :");
            Console.Write("2. DIM 2 :");
            Console.Write("3. DIM 3 :");
            Console.Write("4. DIM 4 :");
            Console.Write("5. DIM 5 :");
            Console.Write("6. DIM 6 :");
            Console.Write("7. DIM 7 :");
            Console.Write("8. DIM 8 :");
            Console.Write("9. DIM 9 :");
            Console.Write("10. DIM 10 :");
            Console.Write("11. Pohon Faktorial :");
            Console.Write("12. Matriks Kotak bolong :");
            Console.Write("13. Deret *3 bintang  :");
            Console.Write("14. Deret *5 -  :");
            Console.Write("15. Fibbonaci 2 :");
            Console.Write("16. Fibbonaci 3 :");

            Console.Write("\n");

            Console.WriteLine("Masukan soal pilihan");
            int pilih = int.Parse(Console.ReadLine());

            switch (pilih)
            {
                case 1:
                    dim1();
                    break;
                case 2:
                    dim2();
                    break;
                case 3:
                    dim3();
                    break;
                case 4:
                    dim4();
                    break;
                case 5:
                    dim5();
                    break;
                case 6:
                    dim6();
                    break;
                case 7:
                    dim7();
                    break;
                case 8:
                    dim8();
                    break;
                case 9:
                    dim9();
                    break;
                case 10:
                    dim10();
                    break;
                case 11:
                    pr1();
                    break;
                case 12:
                    pr2();
                    break;
                case 13:
                    pr3();
                    break;
                case 14:
                    pr4();
                    break;
                case 15:
                    pr5();
                    break;
                case 16:
                    pr6();
                    break;


            }
                Console.Write("\nPilih Soal Lagi? (y/n) : ");
                ulang = Console.ReadLine();
                Console.Clear();
            }
            while (ulang.ToUpper() == "Y");

            //SOAL  PERULANGAN 
            // pohon();
            //fibbonaci2();
            //fibbonaci3();
            // pr4();
            // pr3();
            //pr2();
            //pr1();
            //dim1();
            //dim2();
            //dim3();
            //dim4();
            //dim5();
            //dim6();
            //dim7();
            //dim8();
            //dim9();
            //dim10();
            Console.ReadKey();
        }
        static void dim1()
        {
            int n;
            int  i ;
            int a = 1;

            Console.Write("Masukan angka :");
            n = int.Parse(Console.ReadLine());
            

            for (i = 1; i <= n ; i++ )
            {
               
                Console.Write($"{a} ");
                a += 2;
            }

        }

        static void dim2()
        {
            int n = 7;
            int i ;
            int a = 2;

            Console.Write("Masukan angka :");
            n = int.Parse(Console.ReadLine());


            for (i = 1; i <= n; i ++)
            {
                
                Console.Write($"{a} ");
                a += 2;
            }

        }

        static void dim3()
        {
            int n = 7;
            int a = 1;
            int i;

            Console.Write("Masukan angka :");
            n = int.Parse(Console.ReadLine());



            for (i = 1; i <= n; i ++)
            {
                
                Console.Write($"{a} ");
                a += 3;
            }
        }

        static void dim4()
        {
            int n = 7;
            int a = 0;
            int i;

            Console.Write("Masukan angka :");
            n = int.Parse(Console.ReadLine());


            for (i = 1; i <= n; i ++)
            {
                 
                Console.Write($"{a} ");
                a += 4;
            }
        }

        static void dim5()
        {
            int n = 7;
            int a = 1;
            int i;

            Console.Write("Masukan angka :");
            n = int.Parse(Console.ReadLine());


            for (i = 1; i <=  n; i++)
                if (i  % 3 == 0)
            {
                        Console.Write("* ");
                    }
                    else
                    {
                        Console.Write($"{a} ");
                    a += 4;
                          
                    }
        }

        static void dim6()
        {
            int n = 7;
            int a = 1;
            int i;


            Console.Write("Masukan angka : ");
            n = int.Parse(Console.ReadLine());

            for (i = 1; i <= n; i++)
                if (i % 3 == 0)
                {
                    Console.Write("* ");
                    a += 4;
                }
                else
                {
                    Console.Write($"{a} ");
                    a += 4;

                }
        }

        static void dim7()
        {
            int n = 7;
            int a = 2;
            int i;

            Console.Write("Masukan angka :");
            n = int.Parse(Console.ReadLine());


            for (i = 1; i <= n; i++)
            {
                Console.Write(a + " ");
                a *= 2;
            }
        }

        static void dim8()
        {
            int n = 7;
            int a = 3;
            int i;

            Console.Write("Masukan angka :");
            n = int.Parse(Console.ReadLine());


            for (i = 1; i <= n; i++)
            {
                Console.Write(a + " ");
                a *= 3;
            }
        }

        static void dim9()
        {
            int n = 7;
            int a = 4;
            int i;

            Console.Write("Masukan angka :");
            n = int.Parse(Console.ReadLine());


            for (i = 1; i <= n; i++)
                if (i % 3 == 0)
            {
                Console.Write("* ");

                }
                else
                {
                    Console.Write($"{a} ");
                    a *= 4;
                }
        }
        
        static void dim10()
        {
            int n = 7;
            int a = 3;
            int i;

            Console.Write("Masukan angka :");
            n = int.Parse(Console.ReadLine());


            for (i = 1; i <= n; i++)
                if (i % 4 == 0)
                {
                    a *= 3; 
                    Console.Write("XXX ");

                }
                else
                {
                    Console.Write($"{a} ");
                    a *= 3;
                }

        }

        //static void pohon()
        //{
        //    int n = 7; ;
        //    Console.Write("Masukan angka : ");
        //    n = int.Parse(Console.ReadLine());

        //    for (int i = 0; i <= n; i++)
        //    {
        //        for(int j = 0; j <= n; j++)
        //        {
        //            if (i==1 || i == n || j==1 || j== n)
        //            {
        //                Console.WriteLine($"{i}");
        //            }
        //            else
        //            {
        //                Console.Write(" ");
        //            }
        //        }
        //    }
        //    Console.WriteLine(" ");
          
                    
        //}

        static void pr5()
        {
            int n;
            int a = 1;
            int b = 1;
            Console.Write("Masukan angka : ");
            n = int.Parse(Console.ReadLine());

            for(int i = 0; i < n; i++)
            {
                Console.Write($"{a} ");
                int c = a;
                a = b;
                b = c + b;
            }
        
        }

        static void pr6()
        {
            int n;
            int a = 1;
            int b = 1;
            int c = 1;
            
            Console.Write("Masukan angka : ");
            n = int.Parse(Console.ReadLine());

            for (int i = 0; i < n; i++)
            {
                Console.Write($"{a } ");
                int d = a + b+c;

                a = b;
                b = c;
                c = d; 
            }
        }

        static void pr4()
        {
            int n = 7;
            int i;
            int a = 5;
            Console.Write("Masukan angka : ");
            n = int.Parse(Console.ReadLine());

            for (i = 1; i <= n; i++)
            {
                if (i % 2 == 1)
                {


                    Console.Write($" -{a} ");
                    a += 5;


                }
                else
                {

                    Console.Write($"{a}");
                    a += 5;


                }
            }



        }

        static void pr3()
        {
            int n = 7;
            int i;
            int a = 3;
            Console.Write("Masukan angka : ");
            n = int.Parse(Console.ReadLine());

            for (i = 1; i <= n; i++)
            {

                if (i % 2 == 0)
                {

                    Console.Write("*");
                    a *= 3;
                }
                else
                {
                    Console.Write($"{a}");
                    a *= 3;
                }
            }
        }

        static void pr1()
        {
            int n, x;

            Console.Write("masukan angka :");
            n = int.Parse(Console.ReadLine());

            for(int i =2; i<=n; i++)
            {
                while(n %i == 0)
                {
                    x = n / i;
                    Console.WriteLine($"{n}/{i}={x}");
                    n = x;
                }
            }
        }

        static void pr2()
        {
            int angka = 1;


            Console.Write("Masukkan Inputan = ");
            int inputan = int.Parse(Console.ReadLine());
            int n = inputan;
            for (int i = 1; i <= inputan; i++)
            {
                for (int j = 1; j <= inputan; j++)
                {
                    if (i == 1)
                    {
                        Console.Write(angka + " ");
                        angka += 1;
                    }
                    else if (i == inputan)
                    {
                        Console.Write(n + " ");
                        n -= 1;
                    }
                    else if ((i > 1 && i < inputan && j == 1) ||
                            (i > 1 && i < inputan && j == inputan))
                    {
                        Console.Write("* ");
                    }
                    else if (i == j && i > 1 && i < inputan)
                    {
                        Console.Write("# ");
                    }
                    else
                        Console.Write("  ");
                }
                Console.Write("\n");
            }

            //int n;
            //Console.Write("Masukan angka :");
            //n = int.Parse(Console.ReadLine());


            //int a = 1;

            //for (int i = 1; i <= n; i++)
            //{
            //    for (int j = 1; j <= n; j++)
            //    {
            //        if (i == 1)
            //        {
            //            Console.Write(a + " ");
            //            a += 1;

            //        }
            //        else if (i == n)
            //        {
            //            Console.Write(n + " ");
            //            n -= 1;
            //        }
            //        else if ((i > 1 && i < n && j == 1)
            //               || (i > 1 && i < n && j == n))
            //        {
            //            Console.Write("* ");
            //        }
            //        else
            //        {
            //            Console.Write("* ");
            //        }
            //        Console.Write(" ");
            //    }
            //    Console.Write("\n");
            //}


        }
    }

}
