create database DB_HR295

create table tblKaryawan(
id int identity (1,1)not null,
nip varchar (50) not null,
nama_depan varchar(50) not null,
nama_belakang varchar(50) not null,
jenis_kelamin varchar(50) not null,
agama varchar (50) not null,
tempat_lahir varchar(50) not null,
tgl_lahir date,
alamat varchar(100) not null,
pendidikan_terakhir varchar (50) not null,
tgl_masuk date

)

create table tblDivisi(
id int identity (1,1) not null,
kd_divisi varchar (50) not null,
nama_divisi varchar (50) not null
)

create table tblJabatan(
id int identity(1,1) not null,
kd_jabatan varchar(50) not null,
nama_jabatan varchar(50) not null,
gaji_pokok numeric,
tunjangan_jabatan numeric
)

create table tblPekerjaan(
id int identity(1,1) not null,
nip varchar(50) not null,
kode_jabatan varchar(50) not null,
kode_divisi varchar(50) not null,
tunjangan_kinerja numeric,
kota_penempatan varchar(50)
)

select * from tblKaryawan

insert into tblKaryawan(nip,nama_depan,nama_belakang,jenis_kelamin,agama,tempat_lahir,tgl_lahir,alamat,
pendidikan_terakhir,tgl_masuk) values

('001','Hamidi','Samsyudin','Pria','Islam','Sukabumi','1977-04-21','Jl sudirman No 12','S1 Teknik Mesin','2015-12-07'),

('003','Paul','Christian','Pria','Kristen','Ambon','1980-05-27','Jl Veteran No 4','S1 Pendidikan Geografi','2014-02-12'),

('002','Dhandi','Wamida','Wanita','Islam','Palu','1992-01-12','Jl Rambutan No 22','SMA Negeri 02 Palu','2014-12-01')

insert into tblJabatan(kd_jabatan,nama_jabatan,gaji_pokok,tunjangan_jabatan)
values
('MGR','Manager','5500000','1500000'),
('OB','Office Boy','1900000','200000'),
('ST','Staff','3000000','750000'),
('WMGR','Wakil Manager','4000000','1200000')

select * from tblJabatan

insert into tblDivisi(kd_divisi,nama_divisi)
values
('GD','Gudang'),
('HRD','Hrd'),
('KU','Keuangan'),
('UM','Umun')

select * from tblDivisi

insert into tblPekerjaan (nip,kode_jabatan,kode_divisi,tunjangan_kinerja,kota_penempatan)
values 
('001','ST','KU','750000','Cianjur'),
('002','OB','UM','350000','Sukabumi'),
('003','MGR','HRD','1500000','Sukabumi')

select*from tblKaryawan
select * from tblJabatan
select * from tblPekerjaan
select * from tblDivisi

-- 1. Tampilkan nama lengkap, nama jabatan, tunjangan jabatan + gaji , yang gaji + tunjangan pekerjaannya dibawah 5juta

select concat (nama_depan,' ',nama_belakang) as nama_Lengkap,j.nama_jabatan,j.gaji_pokok+j.tunjangan_jabatan as total_tunjangan 
from tblKaryawan as k
join tblPekerjaan as p on k.nip = p.nip
join tblJabatan as j on p.kode_jabatan =j.kd_jabatan
where j.gaji_pokok + j.tunjangan_jabatan < 5000000


-- 2. Tampilkan nama lengkap, jabatan, nama divisi, total gaji, pajak, gaji bersih, yg gendernya pria dan penempatan kerjanya diluar sukabumi

select concat (nama_depan,' ',nama_belakang) as nama_Lengkap,
j.nama_jabatan,d.nama_divisi,
j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja as total_gaji,
(j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja) * 0.05 as pajak,
(j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja)-(j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja) * 0.05 as total_gaji
from tblKaryawan as k
join tblPekerjaan as p on k.nip = p.nip
join tblJabatan as j on p.kode_jabatan =j.kd_jabatan
join tblDivisi as d on d.kd_divisi = p.kode_divisi
where p.kota_penempatan != 'Sukabumi' and k.jenis_kelamin ='Pria'

-- 3. Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus (bonus=25% dari total gaji(gaji pokok+tunjangan_jabatan+tunajangan_kinerja * 7

select k.nip ,concat (nama_depan,' ',nama_belakang) as nama_Lengkap,
j.nama_jabatan,d.nama_divisi,
(((j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja)*7)*0.25) as bonus

from tblKaryawan as k
join tblPekerjaan as p on k.nip = p.nip
join tblJabatan as j on p.kode_jabatan =j.kd_jabatan
join tblDivisi as d on d.kd_divisi = p.kode_divisi

-- 4. Tampilkan nama lengkap, ttal gaji, infak(5%*total gaji) yang mempunyai jabatan MGR
-- Untuk mengaktifkan dukungan pembaca layar, tekan Ctrl+Alt+Z. Untuk mempelajari pintasan keyboard, tekan Ctrl+garis miring.

select k.nip ,concat (nama_depan,' ',nama_belakang) as nama_Lengkap,
j.nama_jabatan,d.nama_divisi,
j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja as total_gaji,
(j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja) * 0.5 as infaq

from tblKaryawan as k
join tblPekerjaan as p on k.nip = p.nip
join tblJabatan as j on p.kode_jabatan =j.kd_jabatan
join tblDivisi as d on d.kd_divisi = p.kode_divisi
where j.kd_jabatan = 'MGR'


-- 5.Tampilkan nama lengkap, nama jabatan, pendidikan terakhir, tunjangan pendidikan(2jt), dan total gaji(gapok+tjabatan+tpendidikan) dimana
select k.nip ,concat (nama_depan,' ',nama_belakang) as nama_Lengkap,
j.nama_jabatan,d.nama_divisi,k.pendidikan_terakhir,
2000000 as tunjangan_pendidikan,
j.gaji_pokok + j.tunjangan_jabatan+2000000 as total_gaji

from tblKaryawan as k
join tblPekerjaan as p on k.nip = p.nip
join tblJabatan as j on p.kode_jabatan =j.kd_jabatan
join tblDivisi as d on d.kd_divisi = p.kode_divisi

where k.pendidikan_terakhir like 'S1%'


-- 6 Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus
select k.nip ,concat (nama_depan,' ',nama_belakang) as nama_Lengkap,
j.nama_jabatan,d.nama_divisi,
case
when p.kode_jabatan = 'MGR'
then (((j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja)*7)*0.25)

when p.kode_jabatan = 'ST'
then (((j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja)*7)*0.25)

else 
(((j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja)*2)*0.25)
end as bonus
from tblKaryawan as k
join tblPekerjaan as p on k.nip = p.nip
join tblJabatan as j on p.kode_jabatan =j.kd_jabatan
join tblDivisi as d on d.kd_divisi = p.kode_divisi

-- 7.Buatlah kolom nip pada table karyawan sebagai kolom unique 
alter table tblKaryawan 
add constraint nip unique(nip)

-- 8. buatlah kolom nip pada table karyawan sebagai index
create index  nip_index on
tblKaryawan (nip)

-- 9. Tampilkan nama lengkap, nama belakangnya diubah menjadi huruf capital dengan kondisi nama belakang di awali dengan huruf W
select nama_depan+' '+upper (nama_belakang)as nama_lengkap 
from tblKaryawan 
where nama_belakang like 'w%'

-- 10.  Perusahaan akan memberikan bonus sebanyak 10% dari total gaji bagi karyawan yg sudah join di peruashaan diatas  8 thn tahun

select k.nip ,concat (nama_depan,' ',nama_belakang) as nama_Lengkap,k.tgl_masuk,j.nama_jabatan,
d.nama_divisi,
j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja as total_gaji,
case
   when DATEDIFF (year,k.tgl_masuk,getdate())  > 8 
then (j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja)*0.10 
else j.gaji_pokok+j.tunjangan_jabatan+p.tunjangan_kinerja

end as bonus,

DATEDIFF (year,k.tgl_masuk,getdate()) as lama_bekerja

from tblKaryawan as k
join tblPekerjaan as p on k.nip = p.nip
join tblJabatan as j on p.kode_jabatan =j.kd_jabatan
join tblDivisi as d on d.kd_divisi = p.kode_divisi
where DATEDIFF (year,k.tgl_masuk,getdate()) >= 8