﻿using System;

namespace logicDay4
{
	class Program
	{
		static void Main(string[] args)
		{
			string ulang = "";
            do
            {
			Console.Write("Masukan Pilihan Soal : ");
			int pilih = int.Parse(Console.ReadLine());

            switch (pilih)
            {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
					case 6:
						pr1();
					break;
					case 7:
						pr2();
					break;
					case 8:
						pr4();
						break;
            }
				Console.Write("\nPilih Soal Lagi? (y/n) : ");
				ulang = Console.ReadLine();
				Console.Clear();
			}
			while (ulang.ToUpper() == "Y");


			//soal2();
			//soal4();
			//soal5();
			Console.ReadKey();
		}
		static void soal1()
		{
			Console.Write("\n");
			Console.Write("Masukan angka : ");
			int n = int.Parse(Console.ReadLine());

			int[] array_sort = new int[n];
			for (int i = 0; i < n; i++)
			{
				Console.Write("urutan " + (i + 1) + "= ");
				array_sort[i] = Convert.ToInt32(Console.ReadLine());
			}
			for (int j = 0; j <= n - 1; j++)
			{
				for (int k = 0; k <= n - 2; k++)
				{
					if (array_sort[k] > array_sort[k + 1])
					{
						int temp = array_sort[k];
						array_sort[k] = array_sort[k + 1];
						array_sort[k + 1] = temp;
					}
				}
			}
			Console.Write("\n");
			Console.WriteLine("Diurutkan secara ASC");
			Console.Write($"hasil :{string.Join(" ", array_sort)}");
		}
	
		static void soal2()
		{

			Console.Write("Masukkan Batas Bilangan Prima : ");
			int bilangan = int.Parse(Console.ReadLine());

			bool prima = true;

			if (bilangan >= 2)
			{
				for (int i = 2; i < bilangan; i++)
				{
					for (int j = 2; j < i; j++)
					{
						if ((i % j) == 0)
						{
							prima = false;
						}
					}
					if (prima)
						Console.Write($"{i} ");
					prima = true;
				}
			}
		}

		static void soal3()
        {
			int total = 0;
			Console.Write("P :");
			int p = int.Parse(Console.ReadLine());

			Console.Write("D :");
			int d = int.Parse(Console.ReadLine());

			Console.Write("M :");
			int m = int.Parse(Console.ReadLine());

			Console.Write("S :");
			int s = int.Parse(Console.ReadLine());

			while(p >= m && s >= p)
            {
				s -= p;
				p -= d;
                if (p < m)
                {
					p = m;
                }
				total++;
            }
			Console.Write($"{total} Games");
        }

        static void soal4()
        {
			Console.Write("Masukan angka : ");
			int n = int.Parse(Console.ReadLine());

			for (int i = 0 ; i < n; i++)
            {
				for (int j = 2 * (n - i); j > 0; j--)
                {
					Console.Write(" ");
                }
				for (int j = 0; j <= i; j++)
                {
					Console.Write($"{i} ");
                }
				Console.Write("\n");
            }
        }

		static void soal5()
        {
			string input;
			Console.Write("Masukan kata : ");
			input = Console.ReadLine();


			int x = 0;

			string hasil = "";
			int sinyalSalah = 0;

			for (x = 0; x < input.Length - 1; x += 3)
			{
				hasil = input.Substring(x, 3);

				if (hasil.ToUpper() == "SOS")
				{
					Console.WriteLine($"sinyal benar = {hasil} ");

				}
				else
				{
					Console.WriteLine($"sinyal salah = {hasil} ");
					sinyalSalah++;
				}
			}
			Console.Write($"Total Sinyal Salah = {sinyalSalah}");
		}

		static void pr1()
		{
			Console.Write("Masukan Kata : ");
			string kalimat = (Console.ReadLine());
			string[] arrKalimat = kalimat.Split(" ");

			for (int i = 0; i < arrKalimat.Length; i++)
			{
				string kata = arrKalimat[i];
				for (int j = 0; j < kata.Length; j++)
				{
					if (j == 0)
					{
						char huruf = kata[j];
						Console.Write($"{huruf}");
					}
					else if (j == 1 || j < kata.Length - 1)
					{
						Console.Write("*");
					}
					else if (j == kata.Length - 1)
					{
						char huruf = kata[j];
						Console.Write($"{huruf} ");
					}
				}

			}
		}

		static void pr2()
		{
			Console.Write("Masukan Kata : ");
			string kalimat = (Console.ReadLine());

			string[] arrKalimat = kalimat.Split(" ");

			for (int i = 0; i < arrKalimat.Length; i++)
			{
				string kata = arrKalimat[i];
				for (int j = 0; j < kata.Length; j++)
				{
					if (j % 2 != 0)
					{
						Console.Write("*");
					}

					else
					{
						char huruf = kata[j];
						Console.Write($"{huruf}");
					}
				}
				Console.Write(" ");
			}
		}

		static void pr3()
		{
			Console.Write("Masukan Kata = ");
			string kata = Console.ReadLine();

			string katabaru = "";
			string pembanding = "";

			for (int i = kata.Length - 1; i >= 0; i--)
			{
				katabaru += kata[i];

				if (katabaru == kata)
				{
					pembanding = "YES";
				}
				if (katabaru != kata)
				{
					pembanding = "NO";
				}
			}
			Console.WriteLine($"Output = {katabaru}");
			Console.WriteLine($"Hasil = {pembanding}");

		}

		static void pr4()
		{


			Console.Write("Masukan input uang yang dimiliki = ");
			int input = int.Parse(Console.ReadLine());

			Console.Write("Masukan harga celana = ");
			string celana = Console.ReadLine();
			string[] arraycelana = celana.Split(",");
			int[] Celana = Array.ConvertAll(arraycelana, int.Parse);




			Console.Write("Masukan harga baju = ");
			string[] baju = Console.ReadLine().Split(",");
			int[] Baju = Array.ConvertAll(baju, int.Parse);

			int maxValue = 0;

			//int[] jumlah = new int[Baju.Length];
			for (int i = 0; i < Baju.Length; i++)
			{
				int temp = Celana[i] + Baju[i];

				if (temp <= input)
				{
					//jumlah[i] = temp;
					if (maxValue < temp)
					{
						maxValue = temp;
					}
				}
			}

			Console.Write($"Kamu dapat membeli baju dan celana dengan total harga Rp.{maxValue}");
		}
	}
	//for (int i = 0; i < b; i++)
	//{
	//	int[] array_baju = new int[b];
	//	Console.Write("Baju " + (i + 1) + "= ");
	//	array_baju[i] = Convert.ToInt32(Console.ReadLine());
	//}

	//for (int j = 0; j < c; j++)
	//{
	//	int[] array_celana = new int[c];
	//	Console.Write("Celana " + (j + 1) + "= ");
	//	array_celana[j] = Convert.ToInt32(Console.ReadLine());
	//}

}


