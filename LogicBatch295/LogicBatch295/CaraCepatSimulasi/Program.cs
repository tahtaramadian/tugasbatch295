﻿using System;

namespace CaraCepatSimulasi
{
    class Program
    {
        static void Main(string[] args)
        {
            //soal1();
            //soal2();
            //soal3();
            //soal6();
            //soal7();
            //soales();
            soalderetselisih();
            Console.ReadKey();
        }
        static void soal1()
        {
            Console.WriteLine("Masukan kalimat : ");
            char[] input = Console.ReadLine().ToCharArray();

            int hasil = 0;

            foreach (char kata in input)
            {
                if (kata.ToString() == kata.ToString().ToUpper())
                {
                    hasil += 1;
                }
            }
            Console.WriteLine(hasil);
        }

        static void soal2()
        {
            string xsis = "XA";
            Console.WriteLine("Masukan Input : ");
            int input = int.Parse(Console.ReadLine());


            Console.WriteLine("Masukan Batas : ");
            int end = int.Parse(Console.ReadLine());

            DateTime tgl = DateTime.Now;


            string date = tgl.ToString("dd");
            string month = tgl.ToString("MM");
            string year = tgl.ToString("yyyy");

            for (int i = input; i <= end; i++)
            {
                string baru = "0000" + i;
                Console.WriteLine($"{xsis}-{date}{month}{year}-{baru}");
            }
        }

        static void soal3()
        {
            Console.WriteLine("Masukan Keranjang1 : ");
            string keranjang1 = (Console.ReadLine());

            Console.WriteLine("Masukan Keranjang2 : ");
            string keranjang2 = (Console.ReadLine());

            Console.WriteLine("Masukan Keranjang3 : ");
            string keranjang3 = (Console.ReadLine());

            Console.WriteLine("Keranjang yang mau dibawa :");
            int keranjangyangdibawa = int.Parse(Console.ReadLine());

            int output = 0;

            if (keranjang1.ToLower() == "kosong")
            {
                keranjang1 = "0";
            }
            if (keranjang2.ToLower() == "kosong")
            {
                keranjang2 = "0";
            }
            if (keranjang3.ToLower() == "kosong")
            {
                keranjang3 = "0";
            }
            if (keranjangyangdibawa == 1)
            {
                output = int.Parse(keranjang2) + int.Parse(keranjang3);
            }
            if (keranjangyangdibawa == 2)
            {
                output = int.Parse(keranjang1) + int.Parse(keranjang3);
            }
            if (keranjangyangdibawa == 3)
            {
                output = int.Parse(keranjang1) + int.Parse(keranjang2);
            }
            Console.Write($"Sisa Buah ={output}");
        }

        static void soal4()
        {
            Console.Write("Laki Dewasa = ");
            int lakiDewasa = int.Parse(Console.ReadLine());
            Console.Write("Wanita Dewasa = ");
            int wanitaDewasa = int.Parse(Console.ReadLine());
            Console.Write("Anak-anak = ");
            int anak = int.Parse(Console.ReadLine());
            Console.Write("Bayi = ");
            int bayi = int.Parse(Console.ReadLine());
            Console.Write("Input baju untuk = ");
            int pilih = int.Parse(Console.ReadLine());
            int total = 0;

            switch (pilih)
            {
                case 1:
                    Console.Write("Laki Dewasa = ");
                    int pilih1 = int.Parse(Console.ReadLine());
                    total = lakiDewasa + (wanitaDewasa * 2) + (anak * 3) + (bayi * 5) + pilih1;
                    break;
                case 2:

                    Console.Write("Wanita Dewasa = ");
                    int pilih2 = int.Parse(Console.ReadLine());
                    total = lakiDewasa + (wanitaDewasa * 2) + (anak * 3) + (bayi * 5) + (pilih2 * 2);
                    break;

                case 3:
                    Console.Write("Anak-anak = ");
                    int pilih3 = int.Parse(Console.ReadLine());
                    total = lakiDewasa + (wanitaDewasa * 2) + (anak * 3) + (bayi * 5) + (pilih3 * 3);
                    break;

                case 4:

                    Console.Write("Bayi = ");
                    int pilih4 = int.Parse(Console.ReadLine());
                    total = lakiDewasa + (wanitaDewasa * 2) + (anak * 3) + (bayi * 5) + (pilih4 * 5);
                    break;
            }

            int totalBaru = 0;

            if (total > 10 && total % 2 > 0)
            {
                totalBaru = total;
            }

            Console.WriteLine($"Total Baju = {totalBaru} Buah");
        }

        static void soal5()
        {
            Console.Write("Input : ");
            string[] input = Console.ReadLine().Split(",");
            int[] nilai = Array.ConvertAll(input, int.Parse);
            int simpan = 0;
            for (int i = 0; i < nilai.Length; i++)
            {
                simpan = nilai[i];
                if (nilai[i] > 35)
                {
                    while (nilai[i] % 5 != 0)
                    {
                        nilai[i]++;
                    }
                    if (nilai[i] - simpan >= 3)
                    {
                        nilai[i] = simpan;
                    }
                }
                Console.WriteLine(nilai[i]);
            }
        }

        static void soal6()
        {
            string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
            Console.Write("Input = ");
            string input = Console.ReadLine();
            int nilai = 0;

            for (int i = 0; i < alphabet.Length; i++)
            {
                int indeks = input.IndexOf(alphabet[i]);
                if (indeks > -1)
                {
                    nilai++;
                }
            }

            if (nilai == 26)
            {
                Console.WriteLine("Kalimat ini adalah  Pangram");
            }
            else
            {
                Console.WriteLine("Kalimat ini adalah bukan Pangram");
            }

        }

        static void soal7()
        {
            int a1 = 1, a2 = 1, a3 = 0, i, input;
            Console.Write("masukan angka : ");
            input = int.Parse(Console.ReadLine());

            Console.Write($"{a1} {a2} ");
            for (i = 2; i < input; ++i)
            {
                a3 = a1 + a2;
                Console.Write(a3 + " ");
                a1 = a2;
                a2 = a3;
            }
            double sumFibo = a1 + a2 + a3 - 1;

            Console.Write("= " + sumFibo + " hasil bagi = ");
            double bagifibo = sumFibo / input;
            Console.WriteLine(bagifibo);
        }


        static void soal8()
        {

        }

        static void soal9()
        {
            Console.Write("Beli Pulsa = ");
            int pulsa = int.Parse(Console.ReadLine());
            int point = 0;
            int rangePoint1 = 0;
            int rangePoint2 = 0;

            if (pulsa <= 10000)
            {
                point = 0;
            }
            if (pulsa <= 30000)
            {
                rangePoint1 = (pulsa - 10000) / 1000;
                point = rangePoint1;
            }
            else
            {
                rangePoint1 = (pulsa - 10000) / 1000;
                if (rangePoint1 >= 20)
                {
                    rangePoint1 = 20;
                }
                rangePoint2 = (pulsa - 30000) / 1000 * 2;
                point = rangePoint1 + rangePoint2;
            }
            Console.WriteLine($"Point Anda adalah = {point} point");
        }

        static void soal10()
        {
            Console.WriteLine("Input I :");
            int inputI = int.Parse(Console.ReadLine());

            Console.WriteLine("Input J : ");
            int inputJ = int.Parse(Console.ReadLine());

            Console.WriteLine("Input K :");
            int inputK = int.Parse(Console.ReadLine());

            string temp1 = "";
            string temp2 = "";

            for (int i = inputI; i <= inputI; i++)
            {

            }


        }
        static void soalpukis()
        {
            double kuePukis = 15;
            double terigu = 125 / kuePukis;
            double gulaPasir = 100 / kuePukis;
            double susuMurni = 100 / kuePukis;
            double putihTelur = 100 / kuePukis;

            Console.WriteLine("Untuk membuat 1 kue pukis  dibutuh kan :");
            Console.WriteLine($"Terigu      = {terigu} gram");
            Console.WriteLine($"Gula Pasir  = {gulaPasir} gram");
            Console.WriteLine($"Susu Murni  = {susuMurni} gram");
            Console.WriteLine($"Putih Telur = {putihTelur} gram");
        }

        static void soalkalimatbintang()
        {
            Console.Write("Input = ");
            string[] arrKalimat = Console.ReadLine().Split(" ");

            for (int i = 0; i < arrKalimat.Length; i++)
            {
                string kata = arrKalimat[i];
                for (int j = 0; j < kata.Length; j++)
                {
                    if (j == 0)
                    {
                        char huruf = kata[j];
                        Console.Write($"{huruf}");
                    }
                    else if (j == 1)
                    {
                        Console.Write("***");
                    }
                    else if (j == kata.Length - 1)
                    {
                        char huruf = kata[j];
                        Console.Write($"{huruf} ");
                    }
                }
            }
        }


        static void soales()
        {
            Console.WriteLine("Masukan jumlah uang : ");
            int uang = int.Parse(Console.ReadLine());

            int hasil = 0;
            int es = 0;
            int total = 0;


            if (uang >= 1000)
            {
                hasil = uang / 1000;
                Console.WriteLine($"Es yang didapat  = {hasil}");
            }
            es = hasil / 5;
            Console.WriteLine($"total bonus {es}");
            total = es + hasil;
            Console.WriteLine($"es yang didapat {total}");


        }

        static void soalderetselisih()
        {
            Console.WriteLine("Masukan Nilai = ");
            string[] input = Console.ReadLine().Split(",");
            int[] nilai = Array.ConvertAll(input, int.Parse);

            int simpan = 0;

            for (int i = 0; i < nilai.Length; i++)
            {
                for (int j = 0; j < input.Length; j++)
                {
                    if(nilai[i] - nilai[j] == 2)
                    {
                        simpan++;
                    }
                }
            }


        }


        static void soalvideogame()
        {
            Console.WriteLine("Input rintagan = ");
            int inputR = int.Parse(Console.ReadLine());

            Console.WriteLine("Input K = ");
            int inputK = int.Parse(Console.ReadLine());

            int simpan = 0;

           


            for (int i = inputR; i <= inputK; i++)
            {
                if(inputR > inputK)
                {
                    simpan += 1;
                }
            }


        }

        static void latihan1()
        {
            Console.Write("Masukkan Uang Bambang = Rp.");
            int uang = int.Parse(Console.ReadLine());

            int esloli = 0;
            int hasil_bagi = 0;

            esloli = uang / 1000;

            hasil_bagi = esloli / 5;

            esloli += hasil_bagi;

            Console.Write(esloli);
        }
        static void latihan2()
        {
            Console.Write("Hasil Selisih = ");
            int hasil_selisih = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Inputan Angka = ");
            string[] inputan = Console.ReadLine().Split(',');
            int[] angka = Array.ConvertAll(inputan, int.Parse);

            int temp = 0;
            int hasil = 0;
            int output = 0;

            for (int i = 0; i < angka.Length; i++)
            {
                temp = angka[i];
                for (int j = 0; j < angka.Length; j++)
                {
                    hasil = temp - angka[j];
                    if (hasil == hasil_selisih)
                    {
                        output += 1;
                    }
                }
            }
            Console.Write(output);
        }
        static void latihan3()
        {
            Console.Write("Maksimum ketinggian karakter dapat melompat = ");
            int max = int.Parse(Console.ReadLine());

            Console.Write("Rintangan = ");
            string[] tingkat = Console.ReadLine().Split(' ');
            int[] tingkatan = Array.ConvertAll(tingkat, int.Parse);

            int ramuan = 0;
            int temp = 0;
            for (int i = 0; i < tingkatan.Length; i++)
            {
                if (max < tingkatan[i])
                {
                    ramuan += 1;
                }
            }
            Console.Write(ramuan);
        }

    }
}
    

