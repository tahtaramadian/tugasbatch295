--create database DBPenerbit295

--create table tblPengarang(
--id int primary key identity(1,1),
--kd_pengarang varchar(7) not null,
--Nama varchar(30) not null,
--Alamat varchar(80) not null,
--Kota varchar(15) not null,
--kelamin varchar(1) not null
--) 

--select * from tblPengarang

--insert into tblPengarang (kd_Pengarang,Nama,Alamat,Kota,Kelamin) values 
--('P0001','Ashadi','Jl.Beo 25','Yogya','P'),
--('P0002','Rian','Jl.Solo 123','Yogya','P'),
--('P0003','Suwadi','Jl.Semangka','Bandung','P'),
--('P0004','Siti','Jl Durian 15','Solo','W'),
--('P0005','Amir','Jl.Gajah 33','Kudus','P'),
--('P0006','Suparman','Jl.Harimau 25','Jakarta','P'),
--('P0007','Jaja','Jl.Singa 7','Bandung','P'),
--('P0008','Saman','Jl.Naga 12','Yogya','P'),
--('P0009','Anwar','Jl.Tidar 6A','Magelang','P'),
--('P0010','Fatmawati','Jl.Renjana 4','Bogor','W')


--create table tblGaji(
--id int primary key identity not null,
--Kd_Pengarang varchar(7) not null,
--Nama varchar(30) not null,
--Gaji decimal(18,4) not null
--)

--select * from tblGaji
--insert into tblGaji (Kd_Pengarang,Nama,Gaji) values
--('P0002','Rian','600000'),
--('P0005','Amir','700000'),
--('P0004','Siti','500000'),
--('P0003','Suwadi','1000000'),
--('P0010','Fatmawati','600000'),
--('P0008','Saman','750000')

--soal 1
 select count(Kd_Pengarang) as jml_pengarang from tblPengarang

--soal 2
select count(kelamin) as jml_kelamin,kelamin from tblPengarang
group by kelamin

--Soal3 
select count(kota) as jml_kota,kota from tblPengarang
group by kota 

--soal4
select count(kota) as jml_kota,kota from tblPengarang
group by kota having count(kota) > 1

--soal5
--select * from tblPengarang where Kd_Pengarang ='P0001' or Kd_Pengarang = 'P0010'
select min(Kd_Pengarang)as tbl_min from tblPengarang
select max(Kd_Pengarang) as tbl_max from tblPengarang

--soal6
--select * from tblGaji where Gaji ='600000' or Gaji = '1000000'
select min(Gaji)as tbl_min, max(Gaji) as tbl_max from tblGaji

--soal7 
select * from tblGaji 
where Gaji > 600000

--soal8
Select sum(gaji)as jml_gaji from tblGaji

--soal9
select * from tblPengarang as p join tblGaji as g
on p.Kd_Pengarang = g.Kd_Pengarang

select p.kota,sum(gaji) as jml_gaji from tblPengarang as p join tblGaji as g
on p.Kd_Pengarang = g.Kd_Pengarang 
group by kota

--soal10
select * from tblPengarang 
where Kd_Pengarang >='P0001' and  Kd_Pengarang <= 'P0006'

--soal 11
select count(Kota) as jml_kota,Kota from tblPengarang
where Kota = 'Yogya' or Kota = 'Solo' or Kota = 'Magelang'
group by Kota

--select * from tblPengarang in ('solo','yogya','magelang')

-- soal 12
select count(Kota) as jml_kota,Kota from tblPengarang
where Kota != 'Yogya'
group by Kota

-- Soal 13
select * from tblPengarang  where Nama like 'A%' order by Kd_Pengarang
select * from tblPengarang  where Nama like '%i' order by Kd_Pengarang 
select * from tblPengarang  where Nama like '__a%' order by Kd_Pengarang
select * from tblPengarang  where Nama not like '%n' order by Kd_Pengarang

-- soal 14
select * from tblPengarang as p join tblGaji as g
on p.Kd_Pengarang = g.Kd_Pengarang 

-- soal 15
select p.kota,g.gaji from tblPengarang as p join tblGaji as g
on p.Kd_Pengarang = g.Kd_Pengarang 
where gaji < 1000000

-- soal 16
alter table tblPengarang alter column Kelamin varchar(10)

-- soal 17
alter table tblPengarang add gelar varchar(12) 

select * from tblPengarang

--soal 18
update tblPengarang set alamat='Jl Cendrawasih',Kota ='Pekanbaru' where Nama= 'Rian'

-- soal 19
create view  vwPengarang as
select Kd_pengarang,Nama,Kota,gaji from tblPengarang

select * from vwPengarang
alter table vwPengarang add gaji

update tblPengarang set Gelar = 'S1'








----soal 15
--select p.kota,g.gaji from tblPengarang as p 
--join tblGaji as g 
--on p.Kd_Pengarang = g.Kd_Pengarang
--where gaji < 1000000

----soal 16
--alter table tblPengarang alter column Kelamin varchar(10)

--select* from tblPengarang

--update tblPengarang set kelamin ='Pria' where kelamin = 'P'
--update tblPengarang set kelamin ='Wanita' where kelamin = 'W'






