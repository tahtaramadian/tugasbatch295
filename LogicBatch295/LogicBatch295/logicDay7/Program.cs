﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace logicDay7
{
    class Program
    {
        static void Main(string[] args)
        {
            //soal1();
            //soal2();
            // soal6();
            //soal4();
            pr1();
            Console.Read();
        }
        static void soal1()
        {
            DateTime masukGedung = new DateTime(2019, 08, 20, 07, 50, 00);
            Console.WriteLine(masukGedung);

            DateTime keluarGedung = new DateTime(2019, 08, 20, 17, 30, 00);
            Console.WriteLine(keluarGedung);

            TimeSpan interval = keluarGedung - masukGedung;

            Console.WriteLine("Interval menit :" + interval.TotalHours);

            int dateTime = (int)interval.TotalMinutes;
            int jam = 3000;
            int bayar = (jam * dateTime) / 60;
            Console.Write(bayar);
        }
        static void soal2()
        {
            int total = 0;

            Console.Write("Masukkan Tanggal Meminjam Buku = ");
            string tanggal_meminjam = Console.ReadLine();
            DateTime pinjam = DateTime.Parse(tanggal_meminjam);

            Console.Write("Masukkan Lama Peminjaman = ");
            int lama_peminjaman = int.Parse(Console.ReadLine());

            Console.WriteLine($"Tanggal Seharusnya Mengembalikan = {pinjam = pinjam.AddDays(lama_peminjaman)}");
            //Add days digunakan untuk menambahkan hari 

            Console.Write("Masukkan Tanggal Mengembalikan Buku = ");
            string tanggal_mengembalikan = Console.ReadLine();
            DateTime kembalikan = DateTime.Parse(tanggal_mengembalikan);

            if (kembalikan > pinjam.AddDays(lama_peminjaman))
            {
                TimeSpan interval = kembalikan - pinjam.AddDays(lama_peminjaman);
                int Interval = (int)interval.TotalDays;
                total = (Interval + lama_peminjaman) * 500;
                Console.WriteLine($"Denda = Rp{total}");
            }
            else
            {
                Console.WriteLine("Denda = Rp.0");
            }
        }

        static void soal3()
        {
            Console.Write("Input : ");
            string tanggalMasuk = Console.ReadLine();
            DateTime mulai = DateTime.Parse(tanggalMasuk);

            Console.Write("Masukan Tanggal Libur : ");
            string[] libur = Console.ReadLine().Split(",");

            for (int i = 1; i <= 11; i++)
            {
                int date = mulai.Day;
                int hari = (int)mulai.DayOfWeek;
                if (hari == 6 || hari == 0)
                {
                    i--;
                }
                else if (Array.IndexOf(libur, date.ToString()) != -1)
                {
                    i--;
                }
                else
                    Console.WriteLine($"Tanggal {date} hari ke {i}.");

                if (i == 11)
                {
                    break;
                }
                mulai = mulai.AddDays(1);

            }
            Console.WriteLine($"Kelas akan ujian pada {mulai}");
        }

        static void soal6()
        {
            int n, fact;
            Console.Write("Masukan angka factorial : ");
            n = int.Parse(Console.ReadLine());

            int a = 1;
            {


                for (int i = 1; i <= n; i++)
                {
                    a = a * i;
                }
                Console.WriteLine($" Hasil ada : {a} cara ");
            }
        }

        static void soal4()
        {
            Console.Write("Input tanggal dan jam (mulai) : ");
            string mulai = Console.ReadLine();
            DateTime dataMulai = DateTime.Parse(mulai);

            Console.Write("Input durasi main : ");
            int durasi = int.Parse(Console.ReadLine());

            Console.Write("Input tambah durasi main : ");
            int tambah = int.Parse(Console.ReadLine());


            TimeSpan jumlah = dataMulai.AddHours(durasi) - dataMulai;
            int harga = jumlah.Hours * 3500;
            Console.WriteLine($"Akan selesai pada {dataMulai.AddHours(durasi)} dan harga yang harus dibayar adalah Rp.{harga}");

            TimeSpan jumlahTambah = dataMulai.AddHours(durasi + tambah) - dataMulai;
            int hargaTambah = jumlahTambah.Hours * 3500;
            Console.WriteLine($"Jika ditambah {tambah} jam maka akan selesai pada {dataMulai.AddHours(durasi + tambah)} dan harga yang harus dibayar sebanyak {hargaTambah}");

        }

        static void soal5()
        {
            Console.Write("Input Antar Customer: ");
            double inputAntarCustomer = double.Parse(Console.ReadLine());
            double tokoCS1 = 2000, cs1cs2 = 500, cs2cs3 = 1500, cs3cs4 = 300;
            double satuLiterBensin = 2500;
            double jarakTempuh = 0;
            double pemakaianBensin = 0;
            if (inputAntarCustomer == 1)
            {
                jarakTempuh = tokoCS1;
                pemakaianBensin = jarakTempuh / satuLiterBensin;
            }
            if (inputAntarCustomer == 2)
            {
                jarakTempuh = (tokoCS1 + cs1cs2);
                pemakaianBensin = jarakTempuh / satuLiterBensin;
            }
            if (inputAntarCustomer == 3)
            {
                jarakTempuh = (tokoCS1 + cs1cs2 + cs2cs3);
                pemakaianBensin = jarakTempuh / satuLiterBensin;
            }
            if (inputAntarCustomer == 4)
            {
                jarakTempuh = (tokoCS1 + cs1cs2 + cs2cs3 + cs3cs4);
                pemakaianBensin = jarakTempuh / satuLiterBensin;
            }
            Console.WriteLine($"Jarak Tempuh = {jarakTempuh / 1000} KM");
            Console.WriteLine($"Bensin = {Math.Round(pemakaianBensin)}");
        }

        static void soal7()
        {
            int ulang1 = 0;
            Console.Write("Masukan Point = "); //poin taruhannya untuk menebak
            int point = int.Parse(Console.ReadLine());
            do
            {
                Console.Write("Masang Taruhan = "); //beeting untuk masang harga tebakan awal
                int masangTaruhan = int.Parse(Console.ReadLine());
                Console.Write("Masukan Jawaban Tebakan Anda U/D = "); // jawaban taruhan ketika yakin
                string answerSure = Console.ReadLine().ToUpper();
                Random randomAngka = new Random(); // Fungsi Random angka di komputer
                int numbers = randomAngka.Next(0, 9); // angka random mulai dari 0-9 yang telah disediakan/ sudah di deteksi
                bool ulangiPermainan = true; // kondisi spesifik data untuk pengulangan permainan
                string password = ""; /// buat memberikan jawaban yang akan di hasilnya
                if (point > 0)
                {
                    //ulangiPermainan = false;
                    if (masangTaruhan <= point) //point ke update
                    {
                        if (numbers > 0)
                        {
                            password = "U";
                        }
                        if (numbers < 0)
                        {
                            password = "D";
                        }
                        if (answerSure == password)
                        {
                            point += masangTaruhan;
                            Console.WriteLine("You Win");
                        }
                        if (answerSure != password)
                        {
                            point -= masangTaruhan;
                            Console.WriteLine("You Lose");
                        }
                        if (numbers == 5)
                        {
                            point = 0;
                            Console.WriteLine("SERI");
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Anda tidak bisa masang taruhan lebih dari = {point}");
                    }

                }
                if (point <= 0)
                {
                    point = 0;
                    Console.WriteLine("Game Over");
                }
                Console.WriteLine($"Point Saat ini = {point}");
                ulang1 = point;// ini untuk perulangan WHILE
            }
            while (ulang1 > 0);
        }


        static void pr1()
        {
            Console.Write("Input Kayu    : ");
            int[] inputarray = Array.ConvertAll(Console.ReadLine().Split(','), int.Parse);
            List<int> input = new List<int>(inputarray);//cara ubah array ke list

            input.Sort();
            List<int> hasil = new List<int>();
            List<int> hasiltemp = new List<int>();
            hasil.Add(input.Count);
            hasiltemp.Add(1);

            while (input.Count != 0)
            {
                int min = input.Min();
                for (int i = 0; i < input.Count; i++)
                {
                    int temp = input[i] - min;
                    input[i] = temp;

                    if (temp != 0)
                    {
                        hasiltemp.Add(temp);
                    }
                    else if (temp == 0)
                    {
                        input.Remove(temp);
                        i--;
                    }
                }
                if (input.Count != 0)
                {
                    hasil.Add(input.Count);
                }
                hasiltemp = new List<int>();
            }
            Console.WriteLine(string.Join(" ", hasil));
        }

    }
}


