create database PTXA

create table biodata(
id int primary key identity(1,1),
first_name varchar(20)not null,
last_name varchar(30)not null,
dob date,
pob varchar(50)not null,
address varchar(255)not null,
gender varchar(1)not null 
)

insert into biodata(first_name,last_name,dob,pob,address,gender)
values
('soraya','rahayu','1990-12-22','Bali','Jl. Raya Kuta,Bali','P'),
('harum','danuary','1990-01-02','Bandung','Jl. Berkah Ramadhan,Bandung','P'),
('melati','marcelia','1991-03-03','Jakarta','Jl. Mawar3,Brebes','P'),
('farhan','Djokrowidodo','1989-10-11','Jakarta','Jl.Bahari Raya,Solo','L')

create table Employee(
id int primary key identity(1,1)not null,
biodata_id int,
nip varchar(5),
status varchar(10),
join_date date,
salary decimal(10,0)

)

insert into Employee (biodata_id,nip,status,join_date,salary)
values
('1','XA001','Permanen','2015-11-01 00:00:00:000','12000000'),
('2','XA002','Kontrak','2017-01-02 00:00:00:000','10000000'),
('3','XA003','Kontrak','2018-08-19 00:00:00:000','10000000')

create table Contact_Person(
id int primary key identity(1,1)not null,
biodata_id int,
type varchar(5),
contact varchar(100)
)

insert into Contact_Person(biodata_id,type,contact)
values
('1','Mail','soraya@gmail.com'),
('1','Phone','085612234567'),
('2','Mail','hanum.danuary@gmail.com'),
('2','Phone','081312345678'),
('2','Phone','087812345678'),
('3','Mail','melati.marcelia@gmail.com')

create table leave(
id int primary key identity(1,1)not null,
type varchar(10),
name varchar(100)
)

insert into leave(type,name)
values 
('Regular','Cuti Tahunan'),
('Khusus','Cuti Menikah'),
('Khusus','Cuti Haji&Umroh'),
('khusus','Melahirkan')

create table employee_leave(
id int primary key identity(1,1),
employee_id int,
period varchar(4),
regular_quota int
)

insert into employee_leave(employee_id,period,regular_quota)
values
('1','2021','16'),
('2','2021','12'),
('3','2021','12')

create table leave_request (
id int primary key identity(1,1)not null,
employee_id int,
leave_id int,
start_date date,
end_date date,
reason varchar(255)

)

insert into leave_request(employee_id,leave_id,start_date,end_date,reason)
values 
('1','1','2021-10-10','2021-10-12','Liburan'),
('1','1','2021-11-12','2021-11-15','Acara Keluarga'),
('2','2','2021-05-05','2021-05-07','Menikah'),
('2','1','2021-09-09','2021-09-13','Touring'),
('2','1','2021-12-20','2021-12-23','Acara keluarga')

select *from biodata
select *from Employee
select * from Contact_Person
select * from leave
select * from employee_leave
select *from leave_request

-- 1. Menampilkan karyawan yang pertama kali masuk.
select top 1 concat (first_name,' ',last_name) as nama_lengkap,e.join_date from biodata as b
join Employee as e
on b.id = e.biodata_id
order by e.join_date asc

-- 2. Menampilkan daftar karyawan yang saat ini sedang cuti. Daftar berisi nomor_induk, nama, tanggal_mulai, lama_cuti dan keterangan.
select concat (first_name,' ',last_name) as nama,e.nip,
lr.start_date as tanggal_mulai,lr.end_date
,datediff(day,lr.start_date,lr.end_date)+1 as lama_cuti,
lr.reason
from biodata as b
join employee as e on b.id = e.biodata_id
join leave_request as lr on e.id = lr.employee_id
where '2021-12-22' between lr.start_date and lr.end_date


-- 3. Menampilkan daftar karyawan yang sudah mengajukan cuti lebih dari 2 kali. Tampilkan data berisi no_induk, nama, jumlah pengajuan .
select e.nip, concat (b.first_name,' ',b.last_name) as nama,
count(lr.employee_id) as jml
from biodata as b
join employee as e on b.id = e.biodata_id
join employee_leave as el on e.biodata_id = el.employee_id
join leave_request as lr on lr.employee_id = el.employee_id
group by e.nip,concat (b.first_name,' ',b.last_name)
having count(lr.employee_id) > 2

--4. Menampilkan sisa cuti tiap karyawan tahun ini, jika di ketahui jatah cuti setiap karyawan tahun ini adalah sesuaidengan quota cuti. Daftar berisi no_induk, nama, quota, cuti ygsudah di ambil dan sisa_cuti
select e.nip, concat (b.first_name,' ',b.last_name) as nama,
DATEDIFF(day,lr.start_date,lr.end_date) as cuti,
el.regular_quota,
case 
when sum(DATEDIFF(day,lr.start_date,lr.end_date)+1) is null 
then 0
else sum(DATEDIFF(day,lr.start_date,lr.end_date)+1)
end as cuti_yang_diambil,
case
when el.regular_quota - sum(DATEDIFF(day,lr.start_date,lr.end_date)+1) is null 
then el.regular_quota
else  el.regular_quota - sum(DATEDIFF(day,lr.start_date,lr.end_date)+1)
end as sisa_cuti
from biodata as b
left join employee as e on b.id = e.biodata_id
left join employee_leave as el on e.biodata_id = el.employee_id
left join leave_request as lr on lr.employee_id = el.employee_id
group by e.nip,concat (b.first_name,' ',b.last_name),DATEDIFF(day,lr.start_date,lr.end_date),el.regular_quota
having e.nip is not null


--5. P  erusahaan akan meberikan bonus bagi karyawan yang sudah bekerja lebih dari 5 tahun sebanyak 1.5 kali gaji. Tampilan No induk, Fullname, Berapa lama bekerja, Bonus, Total Gaji(gaji + bonus)
select e.nip,concat (b.first_name,' ',b.last_name) as nama,
e.join_date,
datediff(year,e.join_date,el.period) as lama_bekerja,
e.salary,e.salary * 1.5 as bonus,
(e.salary*1.5)+e.salary as total
 from biodata as b
join Employee as e on b.id = e.biodata_id
join employee_leave as el on el.employee_id = e.id
where datediff(year,e.join_date,el.period) > 5

--6. Tampilkan nip, nama_lengkap, jika karyawan ada yg berulang tahun di hari ini akan diberikan hadiah bonus sebanyak 5% dari gaji jika tidak ulang tahun maka bonus 0 dan total gaji . Tampilkan No Induk, nama, Tgl lahir , Usia, Bonus, Total Gaji
select e.nip,concat (b.first_name,' ',b.last_name) as nama,
b.dob,
datediff(year,b.dob,'2021-12-22') as usia,
datediff(year,e.join_date,'2021-12-22') as lama_bekerja,
case 
when day(b.dob) = day('2021-12-22') then
e.salary * 0.05 
else 
0
end as bonus,
e.salary + (e.salary * 0.05) as total
from biodata as b
join Employee as e on b.id = e.biodata_id

--7. Tampilkan No Induk, nama, Tgl lahir , Usia. Urutkan biodata dari yg paling muda sampai yg tua
select e.nip,concat (b.first_name,' ',b.last_name) as nama,
b.dob,
datediff(year,b.dob,'2021-12-22') as usia
 from biodata as b
join Employee as e on b.id = e.biodata_id
order by datediff(year,b.dob,'2021-12-22')asc

--8. Tampikan Karyawan yg belum pernah Cuti
select e.nip, concat (b.first_name,' ',b.last_name) as nama
from biodata as b
left join employee as e on b.id = e.biodata_id
left join employee_leave as el on e.biodata_id = el.employee_id
left join leave_request as lr on lr.employee_id = el.employee_id
where lr.employee_id is null and e.nip is not null


--9. Tampikan Nama Lengkap, Jenis Cuti, Durasi Cuti, dan no telp yang sedang cuti
select concat(b.first_name,' ',b.last_name) Nama_Lengkap,
l.name,DATEDIFF(day,lr.start_date,lr.end_date) as Durasi_Cuti
from biodata as b
 join Employee as e
 on b.id = e.biodata_id

 join Contact_Person as c
 on c.biodata_id = e.id

 join employee_leave as el
 on e.biodata_id = el.employee_id

 join leave_request as lr
 on el.employee_id = lr.employee_id

 join leave as l
 on l.id = lr.leave_id

where lr.start_date between '2021/12/20' and '2021/12/23' and c.type = 'PHONE'
group by concat(b.first_name,' ',b.last_name),l.name,DATEDIFF(day,lr.start_date,lr.end_date),c.contact

--10. Tampilkan nama-nama pelamar yang tidak diterima sebagai karyawan
select concat(b.first_name,' ',b.last_name) as nama
  from biodata as b
left join employee  as e
on  b.id = e.biodata_id 
where e.biodata_id is null

--11. buatlah sebuah view yg menampilkan data nama lengkap, tgl lahir dan tmpat lahir , status, dan salary
create view vwdata as
select concat(b.first_name,' ',b.last_name) as nama,b.dob,b.pob,e.status,e.salary from biodata as b
join Employee as e 
on b.id = e.biodata_id

select * from vwdata












--10. Tampilkan nama-nama pelamar yang tidak diterima sebagai karyawan
select concat(b.first_name,' ',b.last_name) as nama from biodata  as b
left join Employee as e
on b.id= e.biodata_id
where e.biodata_id is null
