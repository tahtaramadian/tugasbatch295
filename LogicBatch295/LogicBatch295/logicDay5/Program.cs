﻿using System;
using System.Linq;

namespace logicDay5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Masukan pilih soal :");
            int pilih = int.Parse(Console.ReadLine());

            switch (pilih)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
                case 5:
                    soal5();
                    break;
            }
            // soal1();
            //soal2();

            Console.ReadKey();

        }
        static void soal1()
        {
            string jamAwal;
            Console.Write("Masukan jam :");
            jamAwal = Console.ReadLine().ToUpper();
            string[] jam = jamAwal.Split(":");
            string waktu = jamAwal.Substring(8, 2);

            int jams = int.Parse(jam[0]);

            if (waktu == "PM")
            {

                if (jams == 12)
                {
                    jams = 12;
                }
                else
                {
                    jams += 12;
                }
            }
            else
            {
                if (waktu == "AM")
                {
                    if (jams == 12)
                    {
                        jams -= 12;
                        Console.Write("0");
                    }
                }
            }
            string menit = jamAwal.Substring(2, 6);
            Console.Write($"{jams}{menit}");

        }

        static void soal2()
        {
            Console.Write("Total menu : ");
            int menu = int.Parse(Console.ReadLine());
            Console.Write("Index makanan alergi : ");
            int alergi = int.Parse(Console.ReadLine());
            Console.Write("Harga menu : ");
            string[] hargaMenu = Console.ReadLine().Split(",");
            Console.Write("Jumlah uang elsa : ");
            int uang = int.Parse(Console.ReadLine());

            int[] harga = Array.ConvertAll(hargaMenu, int.Parse);

            int sum = 0;
            for (int i = 0; i < harga.Length; i++)
            {
                sum += harga[i];

            }
            int jumlah = sum - harga[alergi];
            int total = jumlah / 2;
            int sisaUang = uang - total;

            Console.WriteLine();
            Console.WriteLine($"Elsa harus membayar = Rp.{total}");

            if (sisaUang == 0)
            {
                Console.WriteLine("Uang elsa pas");
            }
            else if (uang < total)
            {
                Console.Write($"Uang elsa kurang {sisaUang}");
            }
            else
            {
                Console.WriteLine($"Sisa uang elsa Rp.{sisaUang}");
            }
        }


        static void soal3()
        {
            int n = 3, d1 = 0, d2 = 0;

            int[,] a = {{ 11, 2, 4 },
                            { 4,5,6},
                            { 10,8,-12}};
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == j)
                    {
                        d1 += a[i, j];

                    }
                    if ((i + j) == (n - 1))
                    {
                        d2 += a[i, j];
                    }
                }
            }
            Console.WriteLine("Diagonal ke 1:"
                                + d1);

            Console.WriteLine(" Diagonal ke 2:"
                                      + d2);
            Console.WriteLine($"Perbedaan d1 dan d2 = {d1 - d2}");
        }

        static void soal4()
        {

            int jumlah = 0;
            Console.Write("Lilin : ");
            string[] lilin = Console.ReadLine().Split(" "); //Input dan split
            int[] convertLilin = Array.ConvertAll(lilin, int.Parse);
            //convert to integer
            int maxLilin = convertLilin.Max();

            for (int i = 0; i < convertLilin.Length; i++)
            {
                if (maxLilin == convertLilin[i])
                {
                    jumlah++;
                }

            }
            Console.WriteLine($"Jumlah lilin yang berhasil ditiup : {jumlah}");

        }
        //    Console.Write("Input = ");
        //    string[] tinggiLilin = Console.ReadLine().Split(" ");
        //    int[] lilin = new int[tinggiLilin.Length];
        //    int lilinTertinggi = 0;
        //    int valueSama = 1;


        //    for (int i = 0; i < tinggiLilin.Length; i++)
        //    {
        //        lilin[i] = int.Parse(tinggiLilin[i]);
        //        for (int j = 0; j < lilin.Length; j++)
        //        {
        //            if (lilin[j] > lilinTertinggi)
        //            {
        //                lilinTertinggi = lilin[j];
        //            }
        //        }
        //    }

        //    int indeks = Array.IndexOf(lilin, lilinTertinggi);
        //    if (indeks != -1)
        //    {
        //        valueSama++;
        //    }
        //    Console.WriteLine(valueSama);
        //}

        static void soal5()
        {

        }
    }
}



