﻿using System;
using System.Collections.Generic;

namespace simulasiUjianTahta
{
    class Program
    {
        static void Main(string[] args)
        {
            string ulang = " ";
            do
            {
                Console.Write("Masukan Pilihan soal 1-10 : ");
                int pilih = int.Parse(Console.ReadLine());

                switch (pilih)
                {
                    case 1:
                        soal1();
                        break;
                    case 2:
                        soal2();
                        break;
                    case 3:
                        soal3();
                        break;
                    case 4:
                        soal4();
                        break;
                    case 5:
                        soal5();
                        break;
                    case 6:
                        soal6();
                        break;
                    case 7:
                        soal7();
                        break;
                    case 8:
                        soal8();
                        break;
                    case 9:
                        soal9();
                        break;
                    case 10:
                        soal10();
                        break;

                }
                Console.Write("\nPilih Soal Lagi? (y/n) : ");
                ulang = Console.ReadLine();
                Console.Clear();

                Console.ReadKey();
            }
            while (ulang.ToUpper() == "Y");
        }

        static void soal1()
        {
                string kata;
                string alphabetbesar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                int output = 0;

                Console.Write("Masukkan Kata = ");
                kata = Console.ReadLine();

                for (int i = 0; i < kata.Length; i++)
                {
                    int index = alphabetbesar.IndexOf(kata[i]);
                    if (index > -1)
                    {
                        output += 1;
                    }
                }
                Console.Write(output);
            }
        static void soal2()
        {

        }
        static void soal3()
        {

        }
        static void soal4()
        {

            Console.WriteLine("Input menu  : ");
            int menu = int.Parse(Console.ReadLine());

            int total = 0;
            int bajuDewasa = 1, bajuWanita = 2, anakAnak = 3, bayi = 2;
            total = bajuDewasa + bajuWanita + anakAnak + bayi;

            switch (menu)
            {
                case 1:
                    Console.WriteLine("Baju Laki laki Dewasa ");
                    int baju1 = int.Parse(Console.ReadLine());
                    baju1 *= bajuDewasa;
                    break;
                case 2:
                    Console.WriteLine("Baju Wanita Dewasa ");
                    int baju2 = int.Parse(Console.ReadLine());
                    baju2 *= bajuWanita;
                    break;
                case 3:
                    Console.WriteLine("Baju Anak anak :");
                    int baju3 = int.Parse(Console.ReadLine());
                    baju3 *= anakAnak;


                    break;
                case 4:
                    Console.WriteLine("Bajua Bayi :");
                    int baju4 = int.Parse(Console.ReadLine());
                    baju4 *= bayi;
                    break;

            }


        }

        static void soal5()
        {
            Console.WriteLine("Masukan Nilai = ");
            string[] arrNilai = Console.ReadLine().Split(","); 
            double[] nilai = new double[arrNilai.Length]; 
            for (int i = 0; i < arrNilai.Length; i++)
            {
                nilai[i] = double.Parse(arrNilai[i]);
            }
            double[] nilaiBaru = new double[nilai.Length];

            for (int i = 0; i < nilai.Length; i++)
            {
                if (nilai[i] < 35)                            
                {                                              
                    nilaiBaru[i] = nilai[i];
                    Console.WriteLine(nilaiBaru[i]);
                }
                else
                {
                    if (nilai[i] % 5 < 3)
                    {
                        nilaiBaru[i] = nilai[i];
                        Console.WriteLine(nilaiBaru[i]);
                    }
                    else
                    {
                        nilaiBaru[i] = Math.Ceiling(nilai[i] / 5);
                        nilaiBaru[i] *= 5;
                        Console.WriteLine(nilaiBaru[i]);
                    }
                }
            }
            Console.WriteLine(" ");
            Console.WriteLine(string.Join(" ",nilaiBaru));

        }
    

        static void soal6()
                {
                    string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ";
                    Console.Write("Input = ");
                    string[] input = Console.ReadLine().Split("");
                    int nilai = 0;

                    for (int i = 0; i < input.Length; i++)
                    {
                        int indeks = alphabet.IndexOf(input[i]);
                        if (indeks != -1)
                        {
                            nilai++;
                        }
                    }

                    if (nilai > 0)
                    {
                        Console.WriteLine("Kalimat ini bukan Pangram");
                    }
                    else if (nilai == 0)
                    {
                        Console.WriteLine("Kalimat ini adalah Pangram");
                    }

                }

        static void soal7()
                {
                    int n;
                    int a = 1;
                    int b = 1;
            int genap, ganjil;

                    Console.Write("Masukan Fibbonaci : ");
                    n = int.Parse(Console.ReadLine());

                    for (int i = 0; i < n; i++)
                    {
                        int c = a;
                        a = b;
                        b = c + b;
                Console.WriteLine($"Bilangan fibbonaci = {a}\n");
                    }
            Console.WriteLine("");
            for (int j = 1;j < n; j++)
            {
                if  (n % 2 == 0)
                {
                    Console.WriteLine($"adalah genap {n}");
                }
                else
                {
                    Console.WriteLine($"adalah ganjil {n}");
                }
            }
                    
                       
                }

        static void soal8()
        {
            Console.WriteLine("Input angka : ");
            int n = int.Parse(Console.ReadLine());


        }

    

        static void soal9()
                {
                    Console.Write("Beli Pulsa = ");
                    int pulsa = int.Parse(Console.ReadLine());
                    int point = 0;
                    int rangePoint1 = 0;
                    int rangePoint2 = 0;

                    if (pulsa <= 10000)
                    {
                        point = 0;
                    }
                    if (pulsa <= 30000)
                    {
                        rangePoint1 = (pulsa - 10000) / 1000;
                        point = rangePoint1;
                    }
                    else
                    {
                        rangePoint1 = (pulsa - 10000) / 1000;
                        if (rangePoint1 >= 20)
                        {
                            rangePoint1 = 20;
                        }
                        rangePoint2 = (pulsa - 30000) / 1000 * 2;
                        point = rangePoint1 + rangePoint2;
                    }
                    Console.WriteLine($"Point Anda adalah = {point} point");
                }
         static void soal10()
                {


                }


    }
}
    


