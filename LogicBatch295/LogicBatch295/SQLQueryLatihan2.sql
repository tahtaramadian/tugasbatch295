--create database DB_Entartainer295

--create table tblArtis(
--id int primary key identity(1,1) not null,
--kd_artis varchar(100) not null,
--nm_artis  varchar(100) not null,
--jk varchar(100) not null,
--bayaran decimal(18,2) not null,
--award int not null,
--negara varchar(100) not null


--)

select * from tblArtis

--insert into tblArtis(kd_artis,nm_artis,jk,bayaran,award,negara) values 

--('A001','ROBERT DOWNEY JR','PRIA','3000000000','2','AS'),
--('A002','ANGELENIE JOLIE','WANITA','700000000','1','AS'),
--('A003','JACKIE CHAN','PRIA','200000000','1','HK'),
--('A004','JOE TASLIM','PRIA','350000000','1','ID'),
--('A005','CHELSEA ISLAN','WANITA','300000000','0','ID')



create table tblFilm (
id int primary key identity(1,1),
kd_film varchar(10) not null,
nm_film varchar(55) not null,
genre varchar(55) not null,
artis varchar(55) not null,
produser varchar(55) not null,
pendapatan decimal(18,2)not null,
nominasi int not null
)
select * from tblFilm

insert into tblFilm(kd_film,nm_film,genre,artis,produser,pendapatan,nominasi) values
('F001','IRON MAN','G001','A001','PD01','2000000000','3'),
('F002','IRON MAN 2','G001','A001','PD01','1800000000','2'),
('F003','IRON MAN 3','G001','A001','PD01','1200000000','0'),
('F004','AVEBGER CIVIL WAR','G001','A001','PD01','2000000000','1'),
('F005','SPIDERMAN HOME COMING','G001','A001','PD01','1300000000','0'),
('F006','THE RAID','G001','A004','PD03','800000000','5'),
('F007','FAST & FURIOUS','G001','A004','PD05','830000000','2'),
('F008','HABIBIE DAM AINUN','G004','A005','PD03','670000000','4'),
('F009','POLICE STORY','G001','A003','PD02','700000000','3'),
('F010','POLICE STORY 2','G001','A003','PD02','710000000','1'),
('F011','POLICE STOIRY 3','G001','A003','PD02','615000000','0'),
('F012','RUSH HOUR','G003','A003','PD05','695000000','2'),
('P013','KUNGFU PANDA','G003','A003','PD05','923000000','5')

create table tblProduser(
id int primary key identity(1,1),
kd_produser varchar(50) not null,
nm_produser varchar(50)not null,
international varchar(50) not null
)

select * from tblProduser

insert into tblProduser(kd_produser,nm_produser,international) values
('PD01','MARVEL','YA'),
('PD02','HONGKONG CINEMA','YA'),
('PD03','RAPI FILM','TIDAK'),
('PD04','PARKIT','TIDAk'),
('PD05','PARAMOUNT PICTURE','YA')

create table tblNegara(
id int primary key identity(1,1),
kd_negara varchar(100) not null,
nm_negara varchar(100) not null
)

insert into tblNegara(kd_negara,nm_negara) values
('AS','AMERIKA SERIKAT'),
('HK','HONGKONGH'),
('ID','INDONESIA'),
('IN','INDIA')

create table tblGenre(
id int primary key identity(1,1),
kd_genre varchar(50) not null,
nm_genre varchar(50) not null
)

insert into tblGenre(kd_genre,nm_genre) values
('G001','ACTION'),
('G002','HORROR'),
('G003','COMEDY'),
('G004','DRAMA'),
('G005','THRILLER'),
('G006','FICTION')

SELECT * from tblFilm,tblProduser

-- Menampilkan jumlah pendapatan produser marvel secara keseluruhan
select p.nm_produser,sum(f.pendapatan) as pendapatan from tblProduser as p
join tblFilm as f
on p.kd_produser = f.produser
group by p.nm_produser having nm_produser ='MARVEL'

--Menampilkan nama film dan nominasi yang tidak mendapatkan nominasi
select * from tblFilm

select nm_film,nominasi from tblFilm
where nominasi = '0'

-- Menampilkan nama film dan pendapatan yang paling tinggi

--select nm_film, max(pendapatan) as jml_tertinggi from tblFilm
--group by nm_film having nm_film = 'IRON MAN'

select *  from  tblFIlm where pendapatan =(select max(pendapatan)from tblFilm)


-- Menampilkan nama film yang huruf depannya 'p'
select nm_film from tblFilm where nm_film Like'P%' order by nm_film

-- Menampilkan nama film yang huruf terakhir 'y'
select nm_film from tblFilm where nm_film Like'%Y' order by nm_film

--Menampilkan nama film yang mengandung huruf 'd'
select nm_film from tblFilm where nm_film Like'%d%' order by nm_film

-- Menampilkan nama film dan artis
select * from tblFilm,tblArtis

select f.nm_film,a.nm_artis from tblFilm as f
join tblArtis as a
on f.artis =  a.kd_artis


-- Menampilkan nama film yang artisnya berasal dari negara hongkong
select * from tblArtis,tblFilm

select f.nm_film,a.negara from tblFilm as f
join tblArtis as a
on f.artis = a.kd_artis 
where a.negara = 'HK'

-- Menampilkan nama film yang artisnya bukan berasal dari negara yang mengandung huruf 'o'
select * from tblArtis,tblFilm,tblNegara

select f.nm_film,n.nm_negara from tblArtis as a 
join tblFilm as f
on a.kd_artis = f.artis
join tblNegara as n
on a.negara= n.kd_negara
where nm_negara not like '%o%'

-- Menampilkan nama artis yang tidak pernah bermain film
select * from tblArtis,tblFilm

select a.nm_artis from tblArtis as a
left join tblFilm as f
on a.kd_artis =  f.artis
where  f.artis is null


-- Menampilkan nama artis yang bermain film dengan genre drama
select * from tblGenre,tblFilm

select a.nm_artis,g.nm_genre from tblFilm  as f
join tblGenre as g on f.genre = g.kd_genre
join tblArtis as a on a.kd_artis =  f.artis
where g.nm_genre='DRAMA'

-- Menampilkan nama artis yang bermain film dengan genre Action
select a.nm_artis,g.nm_genre from tblFilm  as f
join tblGenre as g on f.genre = g.kd_genre
join tblArtis as a on a.kd_artis =  f.artis
where g.nm_genre='ACTION'
group by a.nm_artis,g.nm_genre

-- Menampilkan data negara dengan jumlah filmnya
select * from tblNegara 
select*from tblFilm 
select * from tblArtis

select n.kd_negara,n.nm_negara,count(f.nm_film) as jumlah_film from tblArtis as a 
join tblFilm as f
on a.kd_artis = f.artis
right join tblNegara as n
on a.negara= n.kd_negara
 group by n.kd_negara,n.nm_negara

-- Menampilkan nama film yang skala internasional
select * from tblFilm
select * from tblProduser

select f.nm_film from tblProduser as p
join tblFilm as f
on p.kd_produser = f.produser
where p.international = 'YA'

-- Menampilkan jumlah film dari masing2 produser
select p.nm_produser,count(f.nm_film) as jml_film from tblProduser as p
left join tblFilm as f
on p.kd_produser = f.produser
group by p.nm_produser


--select *,
--case
--when bayaran >= 5000000 then byaran * 0.2
--when bayaran <= 5000000 then and bayaran * 0,1
--else 0
--end as pajak,
--case 
--when bayaran >= 5000000 then  bayaran -(bayaran*0.2)
--when bayaran < 5000000 and bayaran > 200000 then *0,1
--else bayaran
--end as total_bayaran
--from tblArtis

--select sum(total_pembayaran) as total_semua from vwPembayaran

