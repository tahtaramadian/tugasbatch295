create database univ_XA295

create table tblMahasiswa (
nim int  primary key not null,
nama varchar(55),
jenis_kelamin varchar(10),
alamat varchar(100)
)

insert into tblMahasiswa(nim,nama,jenis_kelamin,alamat) 
values 
('101','arif','L','JL. Kenangan'),
('102','budi','L','JL. jombang'),
('103','wati','P','JL. Surabaya'),
('104','ika','P','JL. Jombang'),
('105','tono','L','JL. Jakarta'),
('106','iwan','L','JL. Bandung'),
('107','sari','P','JL. Malang')

drop table tblMahasiswa

create table tblMatakuliah (
id int primary key identity(1,1),
kode_mk varchar(55)not null,
nama_mk varchar(55)not null,
sks int not null,
semester int not null
)

insert into tblMatakuliah(kode_mk,nama_mk,sks,semester)
values
('PTI447','Praktikum Basis Data','1','3'),
('TIK342','Praktikum Basis Data','1','3'),
('PTI333','Basis Data Terdistribusi','3','5'),
('TIK123','Jaringan Komputer','2','5'),
('TIK333','Sistem Operasi','3','5'),
('PTI123','Grafika Multimedia','3','5'),
('PTI777','Sistem Informasi','2','3')

create table tblAmbil_mk(
id int primary key identity(1,1) not null,
nim int not null,
kode_mk varchar(55) not null

)

insert into tblAmbil_mk(nim,kode_mk) values
('101','PTI447'),
('103','TIK333'),
('104','PTI333'),
('104','PTI777'),
('111','PTI123'),
('123','PTI999')


select * from tblMahasiswa
select * from tblMatakuliah
select * from tblAmbil_mk

-- 1. Tampilkan nama mahasiswa dan matakuliah yang diambil
select ma.nama,m.nama_mk  from tblMahasiswa  as Ma
join tblAmbil_mk as a on ma.nim = a.nim
join tblMatakuliah as m on m.kode_mk = a.kode_mk 

-- 2. Tampilkan data mahasiswa yang tidak mengambil matakuliah
select ma.nim,ma.nama ,ma.jenis_kelamin,ma.alamat as jml from tblMahasiswa  as Ma
left join tblAmbil_mk as a on ma.nim = a.nim
left  join tblMatakuliah as m on m.kode_mk = a.kode_mk 
where a.nim is null

-- 3. Kelompokan data mahasiswa yang tidak mengambil matakuliah berdasarkan jenis kelaminnya, kemudian hitung banyaknya.
select count(ma.nama) as jml, ma.jenis_kelamin from tblMahasiswa  as Ma
left join tblAmbil_mk as a on ma.nim = a.nim
left  join tblMatakuliah as m on m.kode_mk = a.kode_mk 
where a.nim is null
group by ma.jenis_kelamin


--4. Dapatkan nim dan nama mahasiswa yang mengambil matakuliah beserta kode_mk dan nama_mk yang diambilnya
select ma.nim,ma.nama,m.kode_mk,m.nama_mk from tblMahasiswa  as Ma
join tblAmbil_mk as a on ma.nim = a.nim
join tblMatakuliah as m on m.kode_mk = a.kode_mk 


-- 5. Dapatkan nim, nama, dan total sks yang diambil oleh mahasiswa, Dimana total sksnya lebih dari 4 dan kurang dari 10.
select ma.nim,ma.nama,sum(m.sks) as jml from tblMahasiswa  as Ma
join tblAmbil_mk as a on ma.nim = a.nim
join tblMatakuliah as m on m.kode_mk = a.kode_mk 
group by ma.nim,ma.nama 
having sum(m.sks) > 4 and sum(m.sks) < 10

-- 6. Dapatkan data matakuliah yang tidak diambil oleh mahasiswa terdaftar (mahasiswa yang terdaftar adalah mahasiswa yang tercatat di tabel mahasiswa).
select m.nama_mk from tblMahasiswa  as Ma
right join tblAmbil_mk as a on ma.nim = a.nim
right  join tblMatakuliah as m on m.kode_mk = a.kode_mk 

where  ma.nama is null
